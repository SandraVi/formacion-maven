package com.formacion.maven.principal.ejercicios;

public class Factorial {

	public void main(String[] args) {
		// hicieron todo el cÃ³digo de factorial

		long result = factorial(7);
		// impresion
	}

	// un mÃ©todo que calcule el factorial
	public static long factorial(long a) {
			
			long factorial = 1;
			
			if(a == 0 || a == 1) {
				return factorial;
			}
					
			for(int i=1; i <= a ; i++) {
				factorial = factorial * i;
			}
			
			return factorial;
		}
}
