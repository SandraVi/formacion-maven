package com.formacion.maven.principal.ejercicios;

public class Matematicas {
	
	public static boolean esNumeroPrimo(int numero) {
		if (numero < 2) {
			return false;
		} else {
			return esPrimo(numero);
		}
	}

	public static int suma(int a, int b) {
		return a + b;
	}

	private static boolean esPrimo(int num) {
		int cont, i;
		cont = 0;
		i = 0;
		for (i = 2; i <= num; i++) {
			if (num % i == 0) {
				cont++;
			}
		}

		if (cont >= 2) {
			return false;
		} else {
			return true;
		}
	}

}
