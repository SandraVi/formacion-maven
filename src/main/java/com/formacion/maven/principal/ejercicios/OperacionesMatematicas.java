package com.formacion.maven.principal.ejercicios;

public class OperacionesMatematicas {

		static int suma(int n1, int n2) {
			int resultado;
			resultado = n1 + n2;
			return resultado;
		}

		static int resta(int n1, int n2) {
			int resultado;
			resultado = n1 - n2;
			return resultado;
		}

		static int multiplicar(int n1, int n2) {
			int resultado;
			resultado = n1 * n2;
			return resultado;
		}

		static double dividir(int n1, int n2) {
			double resultado;
			resultado = n1 / n2;
			return resultado;
		}

	}

