package com.formacion.maven.principal.ejercicios;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class OperacionesMatematicasTest {

	@Test
	public void validarSuma()
	{
		int a = 5;
		int b = 6;
		int resultado = OperacionesMatematicas.suma(a, b);
		assertEquals(11,resultado);
	}
	
	@Test
	public void validarResta()
	{
		int a = 10;
		int b = 6;
		int resultado = OperacionesMatematicas.resta(a, b);
		assertEquals(4,resultado);
	}
	
	@Test
	public void validarMultiplicar()
	{
		int a = 10;
		int b = 6;
		int resultado = OperacionesMatematicas.multiplicar(a, b);
		assertEquals(60,resultado);
	}
	
	@Test
	public void validarDividir()
	{
		int a = 10;
		int b = 5;
		double resultado = OperacionesMatematicas.dividir(a, b);
		assertEquals(2,resultado,0.0);
	}
}
